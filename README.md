# IOT-8266 SmartSwitch

Hi All,
 
 
I would like to share my FIRST "Hello world with IoT-Device".
 
 
Objective: Over internet switching **LED** on/off, connected to IoT-Device through mobile app. Yes ... Smart switch!
 
 
 
This "Hello world with IoT-Device programing" (simple prototype), may help you to get hands on experience/familiarity with IoT(Device,Programing). Of course this is not just limited to turn on/off LED remotely, could be anything (plant watering, Sensor devices(Temperature, Light ..), on/off washing machine, changing fish tank water etc). Our own Smart solutions :-). This is similar to "sonoff smart switch".
 
 
 
How it works: LED <-- IoT Device --> Blynk lib -->    { Cloud (Blynk  Server) }     <-- Blynk App 
 
                        https://docs.blynk.cc/  (link1)
 
 
 
Technology stack:  IoT Device(ESP8266), Blynk App, Blynk Server (Cloud based), Ardunio IDE, C language(any one of the programing knowledge is enough for updating code - ie Wifi user/pwd, Blynk token etc)
 
 
 
Please refer attached page for more details.
 
 
 
Required Hardware/Software :
 
I have shared (external) links for setups/configurations.
 
 
 
1. IoT device- Wemos D1 R2 Wifi(or Wemos D1 R1)  -  https://wiki.wemos.cc/products:d1:d1  (link2)
 
 It is compatible with Ardunio device.
 
 Avaialbe in all leading ecommerce portals  - priced around Rs.500.
 
 I bought - "xcluma Wemos D1 R1 Wifi with ESP8266 Development board", but advanced products/models are available like ESP32, Raspberry Pi. It is better to buy IOT device with development kit (ie Device + USB cable,Breadboard,LED,Jumper Wire etc )
 
Before proceeding to next step, please make sure device is working fine.(1st approach)
 
  
 
2. Blynk App(Blynk-IoT for Ardunio)- Avaialble in Play Store/iStore . https://blynk.io/  (link3)
 
 Acutally there are 3 componentsts - Blynk App, Blynk Server , Blynk Libraries
 
Blynk App - available for iOS/Android with  IOT platform support (similar to AWS IoT crore , Azure IoT Hub etc) .Through this app we can switch LED on/off .
 
  
 
3. Ardunio IDE - Both installable , Web based IDE are available.
 
    https://www.arduino.cc/en/main/software    (link4)
 
    This is used for writing code (sketch) and upload(flashing) code to device.
 
 
 
Please follow the steps as in below link for IDE, Blynk app setup.
 
    https://www.instructables.com/id/Arduino-WeMos-D1-Wifi-UNO-ESP-8266-IoT-IDE-Compati/ (link5)
 
 
 
 How to upload code:
 
  https://learn.adafruit.com/ladyadas-learn-arduino-lesson-number-1/upload-your-first-sketch   (link6)
 
  
 
 Based on complexity, I have explained three approaches [1. Low 2. Medium 3. High]
 
 
 
 1. On board LED on/off program(LED blink program) - Blynk App, Internet(Wifi) are NOT required.
 
    Running simple blink LED program which has less dependecies makes our learing process also simple. Code is avaivalble in IDE itself , 
 
File -> Examples -> Basics -> Blink . Refer "link6" for uploading code. 
 
    This approach make sure that device is working fine and also we are getting familiarity with Arduino IDE.
 
If this is working fine we can try next 2nd/3rd approach, if any issues refer "link8".
 
 
 
 2. On board LED on/off through App (Blynk App, Internet(Wifi) are required)
 
NO - wires,additional external LED, connections are required, simply using on board LED on/off using Blynk app.
 
Note: for default on board LED use pin D5.(default pin no may vary based on board version/model) . In App, "Button settings" select output D5.
 
 
 
 3. External LED is connected(pin D3) to board (Additional LED, Wire, Blynk App, Internet(Wifi) are required)
 
    In Blynk portal, required code is avaialbe https://examples.blynk.cc/?board=ESP8266&shield=ESP8266%20Wifi&example=Widgets%2FLED%2FLED_StatusOfButton  (link7)
 
   
 
    Copy & paste in IDE.
 
 
 
   Update in code 1. Wifi details(ssid , pass),
 
           2. Blynk "auth" in code.(Blynk token - available in your email, as given at the time "Blynk account" sign up)
 
 
 
Follow the 2 steps 
 
1.  In IDE Goto Sketch -> Upload 
 
if everything works fine you could see "Done Uploading" message.
 
 2. In App,  in "Button settings" select output D3.
 
  This pin D3 is referred in two places 
 
          1. in code "Widget LED led3(V3)", 
 
          2. In App "Button Settings" - Output D3 .
 
 
 
Now it is time to play around with our Smart switch, in app press button for on/off LED, Happy Learning :-).
 
 
 
If 1st approach itself NOT working make sure,           (link8)
 
1. Device has no issues - on board LEDs are burning properly after powered
 
2. USB data cable (it is used for power and transfer code) - some cable may not support data transfer.
 
3. CH340 driver installed - usb to serial driver  , In windows - Control panel/Device Manger port is avaialbe(com3 or com4... etc)
 
4. In IDE Tools -> Board (WeMos D1 R2 or  connected device model)
 
                   Port selected correctly 
 
  
 
Still working on 
 
1. Wifi manager - Updating Wifi credentials dynamically instead of hardcode.
 
2. Trying for AWS-IOT platform (AWS IOT core)
 
 
 
Please feel free to share your suggestions/queries.
 
 
 
Happy Learning :-)